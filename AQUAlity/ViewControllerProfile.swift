//
//  ViewControllerProfile.swift
//  AQUAlity
//
//  Created by Nelson on 27/06/2018.
//  Copyright © 2018 Nelson. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseStorage

class ViewControllerProfile: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var emailLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let userEmail = Auth.auth().currentUser?.email
        
        emailLbl.text = userEmail

        let storage = Storage.storage().reference(forURL: "gs://aquality-e07c2.appspot.com/users/")
        let userID = Auth.auth().currentUser!.uid
        let imageName = userID + ".jpg"
        let imageURL = storage.child(imageName)
        
        imageURL.downloadURL(completion: { (url, error) in
            
            if error != nil {
                print(error?.localizedDescription)
                return
            }
            
            URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
                
                if error != nil {
                    print(error)
                    return
                }
                
                guard let imageData = UIImage(data: data!) else { return }
                
                DispatchQueue.main.async {
                    self.imageView.image = imageData
                }

            }).resume()
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func logoutBtn(_ sender: UIButton) {
        do{
            try Auth.auth().signOut()
        }
        catch {
            let alert = UIAlertController(title: "Alert", message: "Error", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default
                ))
            self.present(alert, animated: true, completion: nil)
        }
    }
    

}
