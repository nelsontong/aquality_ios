//
//  ViewControllerSite2.swift
//  AQUAlity
//
//  Created by Nelson on 27/06/2018.
//  Copyright © 2018 Nelson. All rights reserved.
//

import UIKit

class ViewControllerSite2: UIViewController {
    
    var dataDict = [String: String]()
    
    @IBOutlet weak var channelWidth: UITextField!
    @IBOutlet weak var wetWidth: UITextField!
    @IBOutlet weak var averageDepth: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
        
        dataDict["channelWidth"] = channelWidth.text!
        dataDict["wetWidth"] = wetWidth.text!
        dataDict["averageDepth"] = averageDepth.text!
        
        // Create a new variable to store the instance of PlayerTableViewController
        let destinationVC = segue.destination as! ViewControllerSite3
        destinationVC.dataDict = self.dataDict
    }
}
