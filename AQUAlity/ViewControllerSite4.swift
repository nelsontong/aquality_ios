//
//  ViewControllerSite4.swift
//  AQUAlity
//
//  Created by Nelson on 05/07/2018.
//  Copyright © 2018 Nelson. All rights reserved.
//

import UIKit

class ViewControllerSite4: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    var dataDict = [String: String]()
    
    var siltation = ""
    
    @IBOutlet weak var siltationPicker: UIPickerView!
    @IBOutlet weak var mudDepth: UITextField!
    
    var siltationPickerData: [String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.siltationPicker.dataSource = self
        self.siltationPicker.delegate = self
        
        siltationPickerData = ["None", "Present", "Moderate", "Abundant"]
        
        siltation = siltationPickerData[0] as String
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return siltationPickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return siltationPickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        siltation = siltationPickerData[row]
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
        
        dataDict["siltation"] = siltation
        dataDict["mudDepth"] = mudDepth.text!
        
        // Create a new variable to store the instance of PlayerTableViewController
        let destinationVC = segue.destination as! ViewControllerSite5
        destinationVC.dataDict = self.dataDict
    }

}
