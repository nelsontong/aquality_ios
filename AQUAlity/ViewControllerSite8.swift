//
//  ViewControllerSite8.swift
//  AQUAlity
//
//  Created by Nelson on 09/07/2018.
//  Copyright © 2018 Nelson. All rights reserved.
//

import UIKit

class ViewControllerSite8: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var dataDict = [String: String]()

    var shadingDegree = ""
    var suburban = ""
    var moreNatural = ""
    var trees = ""
    var crops = ""
    
    @IBOutlet weak var shadingPicker: UIPickerView!
    @IBOutlet weak var suburbanPicker: UIPickerView!
    @IBOutlet weak var naturalPicker: UIPickerView!
    @IBOutlet weak var treesPicker: UIPickerView!
    @IBOutlet weak var cropPicker: UIPickerView!
    
    var shadingPickerData: [String] = [String] ()
    var suburbanPickerData: [String] = [String] ()
    var naturalPickerData: [String] = [String] ()
    var treesPickerData: [String] = [String] ()
    var cropPickerData: [String] = [String] ()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        shadingPicker.tag = 1
        suburbanPicker.tag = 2
        naturalPicker.tag = 3
        treesPicker.tag = 4
        cropPicker.tag = 5

        
        self.shadingPicker.dataSource = self
        self.shadingPicker.delegate = self
        
        self.suburbanPicker.dataSource = self
        self.suburbanPicker.delegate = self
        
        self.naturalPicker.dataSource = self
        self.naturalPicker.delegate = self
        
        self.treesPicker.dataSource = self
        self.treesPicker.delegate = self
        
        self.cropPicker.dataSource = self
        self.cropPicker.delegate = self
        
        
        shadingPickerData = ["None", "Present", "Moderate", "Abundant"]
        suburbanPickerData = ["None", "Present", "Moderate", "Abundant"]
        naturalPickerData = ["None", "Present", "Moderate", "Abundant"]
        treesPickerData = ["None", "Present", "Moderate", "Abundant"]
        cropPickerData = ["None", "Present", "Moderate", "Abundant"]
        
        shadingDegree = shadingPickerData[0] as String
        suburban = suburbanPickerData[0] as String
        moreNatural = naturalPickerData[0] as String
        trees = treesPickerData[0] as String
        crops = cropPickerData[0] as String
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1 {
            return shadingPickerData.count
        }
        else if pickerView.tag == 2 {
            return suburbanPickerData.count
        }
        else if pickerView.tag == 3 {
            return naturalPickerData.count
        }
        else if pickerView.tag == 4 {
            return treesPickerData.count
        }
        else if pickerView.tag == 5 {
            return cropPickerData.count
        }
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 1 {
            return shadingPickerData[row]
        }
        else if pickerView.tag == 2 {
            return suburbanPickerData[row]
        }
        else if pickerView.tag == 3 {
            return naturalPickerData[row]
        }
        else if pickerView.tag == 4 {
            return treesPickerData[row]
        }
        else if pickerView.tag == 5 {
            return cropPickerData[row]
        }
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1 {
            shadingDegree = shadingPickerData[row] as String
        }
        else if pickerView.tag == 2 {
            suburban = suburbanPickerData[row] as String
        }
        else if pickerView.tag == 3 {
            moreNatural = naturalPickerData[row] as String
        }
        else if pickerView.tag == 4 {
            trees = treesPickerData[row] as String
        }
        else if pickerView.tag == 5 {
            crops = cropPickerData[row] as String
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        dataDict["shading"] = shadingDegree
        dataDict["suburban"] = suburban
        dataDict["natural"] = moreNatural
        dataDict["tree"] = trees
        dataDict["crops"] = crops
        
        let destinationVC = segue.destination as! ViewControllerTimer
        destinationVC.dataDict = self.dataDict
        
    }

}
