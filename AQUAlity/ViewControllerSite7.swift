//
//  ViewControllerSite7.swift
//  AQUAlity
//
//  Created by Nelson on 09/07/2018.
//  Copyright © 2018 Nelson. All rights reserved.
//

import UIKit

class ViewControllerSite7: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    var dataDict = [String: String]()
    
    var clarity = ""
    var velocity = ""
    var riffle = ""
    var rubbish = ""
    
    @IBOutlet weak var clarityPicker: UIPickerView!
    @IBOutlet weak var velocityPicker: UIPickerView!
    @IBOutlet weak var rifflePicker: UIPickerView!
    @IBOutlet weak var rubbishPicker: UIPickerView!
    
    var clarityPickerData: [String] = [String]()
    var velocityPickerData: [String] = [String]()
    var rifflePickerData: [String] = [String]()
    var rubbishPickerData: [String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        clarityPicker.tag = 1
        velocityPicker.tag = 2
        rifflePicker.tag = 3
        rubbishPicker.tag = 4
        
        self.clarityPicker.dataSource = self
        self.clarityPicker.delegate = self
        
        self.velocityPicker.dataSource = self
        self.velocityPicker.delegate = self
        
        self.rifflePicker.dataSource = self
        self.rifflePicker.delegate = self
        
        self.rubbishPicker.dataSource = self
        self.rubbishPicker.delegate = self
     
        clarityPickerData = ["Clear", "Slightly Turbid or Coloured", "Very Turbid or Coloured"]
        velocityPickerData = ["Fast", "Moderate", "Slow"]
        rifflePickerData = ["Riffle", "Glide", "Pool"]
        rubbishPickerData = ["None", "Present", "Moderate", "Abundant"]
        
        clarity = clarityPickerData[0] as String
        velocity = velocityPickerData[0] as String
        riffle = rifflePickerData[0] as String
        rubbish = rubbishPickerData[0] as String
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1 {
            return clarityPickerData.count
        }
        else if pickerView.tag == 2 {
            return velocityPickerData.count
        }
        else if pickerView.tag == 3 {
            return rifflePickerData.count
        }
        else if pickerView.tag == 4 {
            return rubbishPickerData.count
        }
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 1 {
            return clarityPickerData[row]
        }
        else if pickerView.tag == 2 {
            return velocityPickerData[row]
        }
        else if pickerView.tag == 3 {
            return rifflePickerData[row]
        }
        else if pickerView.tag == 4 {
            return rubbishPickerData[row]
        }
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1 {
            clarity = clarityPickerData[row] as String
        }
        else if pickerView.tag == 2 {
            velocity = velocityPickerData[row] as String
        }
        else if pickerView.tag == 3 {
            riffle = rifflePickerData[row] as String
        }
        else if pickerView.tag == 4 {
            rubbish = rubbishPickerData[row] as String
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        dataDict["clarity"] = clarity
        dataDict["velocity"] = velocity
        dataDict["riffle"] = riffle
        dataDict["rubbish"] = rubbish
        
        let destinationVC = segue.destination as! ViewControllerSite8
        destinationVC.dataDict = self.dataDict
        
    }
    
    
}
