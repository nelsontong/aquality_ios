//
//  ViewControllerGroups.swift
//  AQUAlity
//
//  Created by Nelson on 12/07/2018.
//  Copyright © 2018 Nelson. All rights reserved.
//

import UIKit

class ViewControllerGroups: UIViewController {

    var dataDict = [String: String]()
    var insectDict = [String: String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueGroup1" {
            let destinationVC = segue.destination as! ViewControllerGroup1
            destinationVC.dataDict = self.dataDict
            destinationVC.insectDict = self.insectDict
        }
        else if segue.identifier == "segueGroup2" {
            let destinationVC = segue.destination as! ViewControllerGroup2
            destinationVC.dataDict = self.dataDict
            destinationVC.insectDict = self.insectDict
        }
        else if segue.identifier == "segueGroup3" {
            let destinationVC = segue.destination as! ViewControllerGroup3
            destinationVC.dataDict = self.dataDict
            destinationVC.insectDict = self.insectDict
        }
        else if segue.identifier == "segueGroup4" {
            let destinationVC = segue.destination as! ViewControllerGroup4
            destinationVC.dataDict = self.dataDict
            destinationVC.insectDict = self.insectDict
        }
        else if segue.identifier == "segueGroup5" {
            let destinationVC = segue.destination as! ViewControllerGroup5
            destinationVC.dataDict = self.dataDict
            destinationVC.insectDict = self.insectDict
        }
        else if segue.identifier == "segueReview" {
            let destinationVC = segue.destination as! ViewControllerReview
            destinationVC.dataDict = self.dataDict
            destinationVC.insectDict = self.insectDict
        }
    }
    
    @IBAction func reviewBtnPressed(_ : UIButton!) {
        print(dataDict)
    }
    

}
