
//
//  ViewControllerGroup3.swift
//  AQUAlity
//
//  Created by Nelson on 15/07/2018.
//  Copyright © 2018 Nelson. All rights reserved.
//

import UIKit

class ViewControllerGroup3: UIViewController{
    
    var dataDict = [String: String]()
    var insectDict = [String: String]()
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var imageArray = [UIImage]()
        scrollView.frame = view.frame
        
        imageArray = [#imageLiteral(resourceName: "goeridae"), #imageLiteral(resourceName: "hydropsychidae_1"), #imageLiteral(resourceName: "hydroptilidae"), #imageLiteral(resourceName: "limnephlidae_1"), #imageLiteral(resourceName: "polycentropidae_1"), #imageLiteral(resourceName: "rhyacophila_1"), #imageLiteral(resourceName: "sericostomatidae")]
        var imageNameArray = ["Goeridae", "Hydropsychidae", "Hydroptilidae", "Limnephilidae", "Polycentropidae", "Rhycophila", "Sericostomatidae"]
        
        for i in 0 ..< imageArray.count {
            
            let xPosition = self.view.frame.width * CGFloat(i)
            
            scrollView.contentSize.width = scrollView.frame.width * CGFloat(i + 1)
            
            let button = UIButton(type: .custom)
            button.frame = CGRect(x: xPosition, y: 50, width: scrollView.frame.width, height: scrollView.frame.height )
            button.backgroundColor = .clear
            button.addTarget(self, action: #selector(buttonClicked), for: .touchUpInside)
            scrollView.contentSize.width = scrollView.frame.width * CGFloat(i + 1)
            button.setImage(imageArray[i], for: .normal)
            button.imageEdgeInsets = UIEdgeInsetsMake(25, 25, 25, 25)
            button.imageView?.contentMode = UIViewContentMode.scaleAspectFit
            button.tag = i
            
            let label = UILabel(frame: CGRect(x: xPosition, y: self.view.frame.size.height - 100, width: scrollView.frame.width, height: 35))
            label.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            label.textColor = UIColor.black
            label.textAlignment = .center;
            label.font = UIFont(name: "Montserrat-Light", size: 12.0)
            label.text = imageNameArray[i]
            label.alpha = 1.0
            
            scrollView.addSubview(button)
            scrollView.addSubview(label)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func buttonClicked(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            insectInputPopup(key: "Goeridae")
            break;
        case 1:
            insectInputPopup(key: "Hydropsychidae")
            break;
        case 2:
            insectInputPopup(key: "Hydroptilidae")
            break;
        case 3:
            insectInputPopup(key: "Limnephlidae")
            break;
        case 4:
            insectInputPopup(key: "Polycentropidae")
            break;
        case 5:
            insectInputPopup(key: "Rhycophila")
            break;
        case 6:
            insectInputPopup(key: "Sericostomatidae")
            break;
        default: ()
        break;
        }
    }
    
    func insectInputPopup(key: String) {
        let alertController = UIAlertController(title: "Amount", message: "Please input amount found: ", preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: "Confirm", style: .default) { (_) in
            guard let textFields = alertController.textFields, textFields.count > 0
                else {
                    // Could not find textfield
                    return
            }
            
            let field = textFields[0]
            // store your data
            self.dataDict[key] = field.text
            self.insectDict[key] = field.text
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
        
        alertController.addTextField { (textField) in
            textField.placeholder = "Amount"
        }
        
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let destinationVC = segue.destination as! ViewControllerGroups
        destinationVC.dataDict = self.dataDict
        destinationVC.insectDict = self.insectDict
        
    }

}
