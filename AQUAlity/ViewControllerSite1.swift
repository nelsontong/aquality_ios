//
//  ViewControllerSite1.swift
//  AQUAlity
//
//  Created by Nelson on 27/06/2018.
//  Copyright © 2018 Nelson. All rights reserved.
//

import UIKit
import GooglePlaces
import GooglePlacePicker
import GoogleMaps

class ViewControllerSite1: UIViewController, GMSPlacePickerViewControllerDelegate {
    
    @IBOutlet weak var placeLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    
    @IBOutlet weak var streamNameText: UITextField!
    @IBOutlet weak var siteDescriptionText: UITextField!
    @IBOutlet weak var samplerNameText: UITextField!
    
    var dataDict = [String: String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // The code snippet below shows how to create and display a GMSPlacePickerViewController.
    @IBAction func pickPlace(_ sender: UIButton) {
        let config = GMSPlacePickerConfig(viewport: nil)
        
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        
        present(placePicker, animated: true, completion: nil)
        
    }
    
    // To receive the results from the place picker 'self' will need to conform to
    // GMSPlacePickerViewControllerDelegate and implement this code.
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
        
        placeLbl.text = place.name
        addressLbl.text = place.formattedAddress
    
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
        //print("No place selected")
    }
    
   
    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
        
        dataDict["streamName"] = streamNameText.text!
        dataDict["siteDescription"] = siteDescriptionText.text!
        dataDict["samplerName"] = samplerNameText.text!
        dataDict["location"] = placeLbl.text
        
        // Create a new variable to store the instance of PlayerTableViewController
        let destinationVC = segue.destination as! ViewControllerSite2
        destinationVC.dataDict = self.dataDict
    }

    
}

