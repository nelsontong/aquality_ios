//
//  ViewControllerSite3.swift
//  AQUAlity
//
//  Created by Nelson on 04/07/2018.
//  Copyright © 2018 Nelson. All rights reserved.
//

import UIKit

class ViewControllerSite3: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    var dataDict = [String: String]()
    
    var cobble = ""
    var gravel = ""
    var sand = ""
    var silt = ""
    
    @IBOutlet weak var cobblePicker: UIPickerView!
    @IBOutlet weak var gravelPicker: UIPickerView!
    @IBOutlet weak var sandPicker: UIPickerView!
    @IBOutlet weak var siltPicker: UIPickerView!
    
    var cobblePickerData: [String] = [String]()
    var gravelPickerData: [String] = [String]()
    var sandPickerData: [String] = [String]()
    var siltPickerData: [String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        cobblePicker.tag = 1
        gravelPicker.tag = 2
        sandPicker.tag = 3
        siltPicker.tag = 4

        self.cobblePicker.dataSource = self
        self.cobblePicker.delegate = self
        
        self.gravelPicker.dataSource = self
        self.gravelPicker.delegate = self
        
        self.sandPicker.dataSource = self
        self.sandPicker.delegate = self
        
        self.siltPicker.dataSource = self
        self.siltPicker.delegate = self
        
        
        cobblePickerData = ["None", "Present", "Moderate", "Abundant"]
        gravelPickerData = ["None", "Present", "Moderated", "Abundant"]
        sandPickerData = ["None", "Present", "Moderate", "Abundant"]
        siltPickerData = ["None", "Present", "Moderate", "Abundant"]

        cobble = cobblePickerData[0] as String
        gravel = gravelPickerData[0] as String
        sand = sandPickerData[0] as String
        silt = siltPickerData[0] as String
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // The number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1 {
            return cobblePickerData.count
        }
        else if pickerView.tag == 2{
            return gravelPickerData.count
        }
        else if pickerView.tag == 3 {
            return sandPickerData.count
        }
        else if pickerView.tag == 4 {
            return siltPickerData.count
        }
        return 1
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?	 {
        if pickerView.tag == 1 {
            return cobblePickerData[row]
        }
        else if pickerView.tag == 2 {
            return gravelPickerData[row]
        }
        else if pickerView.tag == 3 {
            return sandPickerData[row]
        }
        else if pickerView.tag == 4 {
            return siltPickerData[row]
        }
        return ""
    }
    
    
    // Catpure the picker view selection
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        // This method is triggered whenever the user makes a change to the picker selection.
        // The parameter named row and component represents what was selected.
        if pickerView.tag == 1 {
            cobble = cobblePickerData[row] as String
        }
        else if pickerView.tag == 2 {
            gravel = gravelPickerData[row] as String
        }
        else if pickerView.tag == 3 {
            sand = sandPickerData[row] as String
        }
        else if pickerView.tag == 4 {
            silt = siltPickerData[row] as String
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
        
        //dataDict["streamName"] = streamNameText.text!
        dataDict["cobble"] = cobble
        dataDict["gravel"] = gravel
        dataDict["sand"] = sand
        dataDict["silt"] = silt
        
        // Create a new variable to store the instance of PlayerTableViewController
        let destinationVC = segue.destination as! ViewControllerSite4
        destinationVC.dataDict = self.dataDict
    }
    
}
