//
//  ViewControllerSite5.swift
//  AQUAlity
//
//  Created by Nelson on 06/07/2018.
//  Copyright © 2018 Nelson. All rights reserved.
//

import UIKit

class ViewControllerSite5: UIViewController {

    var dataDict = [String: String]()
    var streamAccess = ""
    
    @IBOutlet weak var fullAccessBtn: UIButton!
    @IBOutlet weak var semiControlledBtn: UIButton!
    @IBOutlet weak var noAccessBtn: UIButton!
    
    @IBOutlet weak var commentText: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fullAccessBtn.tag = 1
        semiControlledBtn.tag = 2
        noAccessBtn.tag = 3

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func accessToStream(_ sender: UIButton) {
        guard let button = sender as? UIButton else {
            return
        }
        
        switch button.tag {
            case 1:
                streamAccess = "Full Access"
            case 2:
                streamAccess = "Semi-Controlled"
            case 3:
                streamAccess = "No Access"
            default:
                streamAccess = "No Access"
        }
        
        // print(streamAccess)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
        
        dataDict["streamAccess"] = streamAccess
        dataDict["comment"] = commentText.text!
        
        // Create a new variable to store the instance of PlayerTableViewController
        let destinationVC = segue.destination as! ViewControllerSite6
        destinationVC.dataDict = self.dataDict
    }
    

}
