//
//  ViewControllerReview.swift
//  AQUAlity
//
//  Created by Nelson on 18/07/2018.
//  Copyright © 2018 Nelson. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseAuth

class ViewControllerReview: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var ref: DatabaseReference!
    
    var dataDict = [String: String] ()
    var insectDict = [String: String]()
    var keys = [String]()
    var values = [String]()
    
    var insectsArr = [[String]]()
    
    let cellReuseIdentifier = "cell"
    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for (key,value) in dataDict {
            keys.append(key)
            values.append(value)
        }
        
        // Register the table view cell class and its reuse id
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        
         self.tableView.tableFooterView = UIView()
        
        // This view controller itself will provide the delegate methods and row data for the table view.
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.keys.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as UITableViewCell!
        
        // set the text from the array
        cell.textLabel?.text = "\(self.keys[indexPath.row]) : \(self.values[indexPath.row]) "
        
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
    }
    
    func sortInsectGroup(data: [String: String]) -> [[String]]{
        for (key, value) in data {
            if key == "Caenis" || key == "Ecdyonurus" || key == "Epherema Danica" || key == "Heptagenia" || key == "Rhithrogena" {
                
                insectsArr.append([key, value, "group1"])
            }
            else if key == "Amphinemura" || key == "Isoperla" || key == "Lauctra" || key == "Perla" || key == "Protonemura" {
                
                insectsArr.append([key, value, "group2"])
            }
            else if key == "Goeridae" || key == "Hydropsychidae" || key == "Hydroptilidae" || key == "Limnephlidae" || key == "Polycentropidae" || key == "Rhycophila" || key == "Sericostomatidae" {
                
                insectsArr.append([key, value, "group3"])
            }
            else if key == "Chironomus" || key == "Dicranota" || key == "Diseniella" || key == "Flatworms" || key == "Leeches" || key == "Lumbriculidae" || key == "Lymnaea" || key == "Naididae" || key == "Planorbis" || key == "Potamopyrgus" || key == "Simuildae"  {
                
                insectsArr.append([key, value, "group4"])
            }
            else if key == "Asellus" {
                
                insectsArr.append([key, value, "group5"])
            }
        }
        return insectsArr
    }
    
    func calculateTotalScore(insects: [[String]]) -> Int {
        
        var group1RelativeAbundance = 0
        var group1Types = 0
        
        var group2RelativeAbundance = 0
        var group2Types = 0
        
        var group3RelativeAbundance = 0
        var group3Types = 0
        
        var group4RelativeAbundance = 0
        var group4Types = 0
        
        var group5RelativeAbundance = 0
        var group5Types = 0
        
        for x in 0..<insects.count {
            if insects[x][2] == "group1" {
                if insects[x][1] != "" && Int(insects[x][1])! > 0 {
                    group1RelativeAbundance += relativeAbundance(numOfEach: Int(insects[x][1])!)
                    group1Types += group1Types + 1
                }
            }
            else if insects[x][2] == "group2" {
                if insects[x][1] != "" && Int(insects[x][1])! > 0 {
                    group2RelativeAbundance += relativeAbundance(numOfEach: Int(insects[x][1])!)
                    group2Types += group2Types + 1
                }
            }
            else if insects[x][2] == "group3" {
                if insects[x][1] != "" && Int(insects[x][1])! > 0 {
                    group3RelativeAbundance += relativeAbundance(numOfEach: Int(insects[x][1])!)
                    group3Types += group3Types + 1
                }
            }
            else if insects[x][2] == "group4" {
                if insects[x][1] != "" && Int(insects[x][1])! > 0 {
                    group4RelativeAbundance += relativeAbundance(numOfEach: Int(insects[x][1])!)
                    group4Types += group4Types + 1
                }
            }
            else if insects[x][2] == "group5" {
                if insects[x][1] != "" && Int(insects[x][1])! > 0 {
                    group5RelativeAbundance += relativeAbundance(numOfEach: Int(insects[x][1])!)
                    group5Types += group5Types + 1
                }
            }
        }
        
        let g1Score = group1Score(numOfTypes: group1Types, relativeAbundance: group1RelativeAbundance)
        
        let g2Score = group1Score(numOfTypes: group2Types, relativeAbundance: group2RelativeAbundance)
        
        let g3Score = group1Score(numOfTypes: group3Types, relativeAbundance: group3RelativeAbundance)
        
        let g4Score = group4Score(numOfTypes: group4Types, relativeAbundance: group4RelativeAbundance)
        
        let g5Score = group1Score(numOfTypes: group5Types, relativeAbundance: group5RelativeAbundance)
        
        let totalScore = g1Score + g2Score + g3Score + g4Score + g5Score
        
        return totalScore
    }
    
    func relativeAbundance(numOfEach: Int) -> Int {
        if numOfEach == 0 {
            return 0
        }
        else if numOfEach >= 1 && numOfEach <= 5 {
            return 1
        }
        else if numOfEach >= 6 && numOfEach <= 20 {
            return 2
        }
        else if numOfEach >= 21 && numOfEach <= 50 {
            return 3
        }
        else if numOfEach >= 51 && numOfEach <= 100 {
            return 4
        }
        else if numOfEach > 100 {
            return 5
        }
        return 0
    }
    
    func group1Score(numOfTypes: Int, relativeAbundance: Int) -> Int {
        if numOfTypes == 0 {
            return 0
        }
        else if numOfTypes == 1 {
            if relativeAbundance >= 1 && relativeAbundance <= 2 {
                return 4
            }
            else if relativeAbundance >= 3 {
                return 6
            }
        }
        else if numOfTypes >= 2 {
            if relativeAbundance == 2 {
                return 4
            }
            else if relativeAbundance >= 3 {
                return 8
            }
        }
        return 0
    }
    
    func group2Score(numOfTypes: Int, relativeAbundance: Int) -> Int {
        if numOfTypes == 0 {
            return 0
        }
        else if numOfTypes == 1 {
            if relativeAbundance >= 1 && relativeAbundance <= 2 {
                return 4
            }
            else if relativeAbundance >= 3 {
                return 6
            }
        }
        else if numOfTypes >= 2 {
            if relativeAbundance == 2 {
                return 6
            }
            else if relativeAbundance >= 3 {
                return 8
            }
        }
        return 0
    }
    
    func group3Score(numOfTypes: Int, relativeAbundance: Int) -> Int {
        if numOfTypes == 0 {
            return 0
        }
        else if numOfTypes >= 1 && numOfTypes <= 2 {
            if relativeAbundance >= 1 && relativeAbundance <= 2 {
                return 2
            }
            else if relativeAbundance >= 3 {
                return 4
            }
        }
        else if numOfTypes >= 3 {
            if relativeAbundance >= 3 {
                return 6
            }
        }
        return 0
    }
    
    func group4Score(numOfTypes: Int, relativeAbundance: Int) -> Int {
        if numOfTypes == 0 {
            return 0
        }
        else if numOfTypes >= 1 && numOfTypes <= 2 {
            if relativeAbundance >= 1 && relativeAbundance <= 6 {
                return 2
            }
            else if relativeAbundance >= 7 {
                return 0
            }
        }
        else if numOfTypes >= 3 {
            if relativeAbundance >= 1 && relativeAbundance <= 6 {
                return 4
            }
            else if relativeAbundance >= 7 {
                return 0
            }
        }
        return 0
    }
    
    func group5Score(numOfTypes: Int, relativeAbundance: Int) -> Int {
        if relativeAbundance == 0 {
            return 6
        }
        else if relativeAbundance >= 1 && relativeAbundance <= 20 {
            return 2
        }
        else if relativeAbundance >= 21 {
            return 0
        }
        return 0
    }
    
    
    @IBAction func uploadToDB(_ sender: UIButton!) {
        
        ref = Database.database().reference()
        
        // get the current date and time
        let currentDateTime = Date()
        
        // initialize the date formatter
        let formatter = DateFormatter()
        formatter.timeStyle = .medium
        formatter.dateStyle = .none
        let time = formatter.string(from: currentDateTime)
        
        let calendar = Calendar.current
        let year = calendar.component(.year, from: currentDateTime)
        let month = calendar.component(.month, from: currentDateTime)
        let day = calendar.component(.day, from: currentDateTime)
        
        let userID = Auth.auth().currentUser?.uid
        
        let key = ref.child("aquality-e07c2").childByAutoId().key
        
        let details = ["Date": "\(day)/\(month)/\(year)",
                       "Time": time,
                       "Uid": userID]
        
        let environment = ["Animals": dataDict["streamAccess"],
                           "Average Depth": dataDict["averageDepth"],
                           "Bog": dataDict["bog"],
                           "Clarity": dataDict["clarity"],
                           "Cobbles": dataDict["cobble"],
                           "Comments": dataDict["comment"],
                           "Crops": dataDict["crops"],
                           "Forest": dataDict["forest"],
                           "Grassland": dataDict["grassland"],
                           "Gravel": dataDict["gravel"],
                           "Location": dataDict["location"],
                           "More Natural": dataDict["natural"],
                           "Mud Depth": dataDict["mudDepth"],
                           "Riffle": dataDict["riffle"],
                           "Rubbish": dataDict["rubbish"],
                           "Samplers": dataDict["samplerName"],
                           "Sand": dataDict["sand"],
                           "Shading": dataDict["shading"],
                           "Silt or Mud": dataDict["silt"],
                           "Siltation": dataDict["siltation"],
                           "Stream Description": dataDict["siteDescription"],
                           "Stream Name": dataDict["streamName"],
                           "Stream Width": dataDict["wetWidth"],
                           "Suburban": dataDict["suburban"],
                           "Tillage": dataDict["tillage"],
                           "Trees": dataDict["tree"],
                           "Urban": dataDict["urban"],
                           "Velocity": dataDict["velocity"],
                           "Wet Width": dataDict["wetWidth"]]
        
        let insectsArr = sortInsectGroup(data: insectDict)
        
        let finalScore = calculateTotalScore(insects: insectsArr)
        
        
        insectDict["AbundanceScore"] = String(finalScore)
        
        let childUpdates = ["/recordings/\(key)/details/": details,
                            "/recordings/\(key)/environment/": environment,
                            "/recordings/\(key)/insects": insectDict]
        
        ref.updateChildValues(childUpdates)
        
        let alertController = UIAlertController(title: "Upload", message: "Data has been uploaded.", preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(defaultAction)
        
        present(alertController, animated: true, completion: nil)
        
        dataDict.removeAll()
    }
    
    
}









