//
//  ViewControllerGroup1.swift
//  AQUAlity
//
//  Created by Nelson on 12/07/2018.
//  Copyright © 2018 Nelson. All rights reserved.
//

import UIKit

class ViewControllerGroup1: UIViewController {

    var dataDict = [String: String]()
    var insectDict = [String: String]()
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var imageArray = [UIImage]()
        scrollView.frame = view.frame
        
        imageArray = [#imageLiteral(resourceName: "caenis_1"), #imageLiteral(resourceName: "ecdyonurus_1"), #imageLiteral(resourceName: "ephemera_danica"), #imageLiteral(resourceName: "heptagenia"), #imageLiteral(resourceName: "rhithrogena_1")]
        var imageNameArray = ["Caenis", "Ecduonurus", "Ephemera Danica", "Heptagenia", "Rhithrogena"]
        
        for i in 0 ..< imageArray.count {
            
            let xPosition = self.view.frame.width * CGFloat(i)
            
            scrollView.contentSize.width = scrollView.frame.width * CGFloat(i + 1)
            
            let button = UIButton(type: .custom)
            button.frame = CGRect(x: xPosition, y: 50, width: scrollView.frame.width, height: scrollView.frame.height - 120 )
            button.backgroundColor = .gray
            button.addTarget(self, action: #selector(buttonClicked), for: .touchUpInside)
            scrollView.contentSize.width = scrollView.frame.width * CGFloat(i + 1)
            button.setImage(imageArray[i], for: .normal)
            button.imageEdgeInsets = UIEdgeInsetsMake(25, 25, 25, 25)
            button.imageView?.contentMode = UIViewContentMode.scaleAspectFill
            button.tag = i
            
            let label = UILabel(frame: CGRect(x: xPosition, y: self.view.frame.size.height - 100, width: scrollView.frame.width, height: 35))
            label.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            label.textColor = UIColor.black
            label.textAlignment = .center;
            label.font = UIFont(name: "Montserrat-Light", size: 12.0)
            label.text = imageNameArray[i]
            label.alpha = 1.0
            
            scrollView.addSubview(button)
            scrollView.addSubview(label)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func buttonClicked(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            insectInputPopup(key: "Caenis")
            break;
        case 1:
            insectInputPopup(key: "Ecdyonurus")
            break;
        case 2:
            insectInputPopup(key: "Ephemera Danica")
            break;
        case 3:
            insectInputPopup(key: "Heptagenia")
            break;
        case 4:
            insectInputPopup(key: "Rhithrogena")
            break;
        default: ()
        break;
        }
    }
    
    func insectInputPopup(key: String) {
        let alertController = UIAlertController(title: "Amount", message: "Please input amount found: ", preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: "Confirm", style: .default) { (_) in
            guard let textFields = alertController.textFields, textFields.count > 0
                else {
                    // Could not find textfield
                    return
            }
            
            let field = textFields[0]
            // store your data
            self.dataDict[key] = field.text
            self.insectDict[key] = field.text
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
        
        alertController.addTextField { (textField) in
            textField.placeholder = "Amount"
        }
        
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let destinationVC = segue.destination as! ViewControllerGroups
        destinationVC.dataDict = self.dataDict
        destinationVC.insectDict = self.insectDict
        
    }

}
