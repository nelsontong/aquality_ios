//
//  ViewControllerSite6.swift
//  AQUAlity
//
//  Created by Nelson on 06/07/2018.
//  Copyright © 2018 Nelson. All rights reserved.
//

import UIKit

class ViewControllerSite6: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    var dataDict = [String: String]()
    
    var grassland = ""
    var tillage = ""
    var urban = ""
    var forest = ""
    var bog = ""
    
    @IBOutlet weak var grasslandPicker: UIPickerView!
    @IBOutlet weak var tillagePicker: UIPickerView!
    @IBOutlet weak var urbanPicker: UIPickerView!
    @IBOutlet weak var forestPicker: UIPickerView!
    @IBOutlet weak var bogPicker: UIPickerView!
    
    var grasslandPickerData: [String] = [String]()
    var tillagePickerData: [String] = [String]()
    var urbanPickerData: [String] = [String]()
    var forestPickerData: [String] = [String]()
    var bogPickerData: [String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        grasslandPicker.tag = 1
        tillagePicker.tag = 2
        urbanPicker.tag = 3
        forestPicker.tag = 4
        bogPicker.tag = 5
        
        self.grasslandPicker.dataSource = self
        self.grasslandPicker.delegate = self
        
        self.tillagePicker.dataSource = self
        self.tillagePicker.delegate = self
        
        self.urbanPicker.dataSource = self
        self.urbanPicker.delegate = self
        
        self.forestPicker.dataSource = self
        self.forestPicker.delegate = self
        
        self.bogPicker.dataSource = self
        self.bogPicker.delegate = self
        
        grasslandPickerData = ["None", "Present", "Moderate", "Abundant"]
        tillagePickerData = ["None", "Present", "Moderated", "Abundant"]
        urbanPickerData = ["None", "Present", "Moderate", "Abundant"]
        forestPickerData = ["None", "Present", "Moderate", "Abundant"]
        bogPickerData = ["None", "Present", "Moderate", "Abundant"]

        
        grassland = grasslandPickerData[0] as String
        tillage = tillagePickerData[0] as String
        urban = urbanPickerData[0] as String
        forest = forestPickerData[0] as String
        bog = bogPickerData[0] as String
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1 {
            return grasslandPickerData.count
        }
        else if pickerView.tag == 2 {
            return tillagePickerData.count
        }
        else if pickerView.tag == 3 {
            return urbanPickerData.count
        }
        else if pickerView.tag == 4 {
            return forestPickerData.count
        }
        else if pickerView.tag == 5 {
            return bogPickerData.count
        }
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 1 {
            return grasslandPickerData[row]
        }
        else if pickerView.tag == 2 {
            return tillagePickerData[row]
        }
        else if pickerView.tag == 3 {
            return urbanPickerData[row]
        }
        else if pickerView.tag == 4 {
            return forestPickerData[row]
        }
        else if pickerView.tag == 5 {
            return bogPickerData[row]
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        // This method is triggered whenever the user makes a change to the picker selection.
        // The parameter named row and component represents what was selected.
        if pickerView.tag == 1 {
            grassland = grasslandPickerData[row] as String
        }
        else if pickerView.tag == 2 {
            tillage = tillagePickerData[row] as String
        }
        else if pickerView.tag == 3 {
            urban = urbanPickerData[row] as String
        }
        else if pickerView.tag == 4 {
            forest = forestPickerData[row] as String
        }
        else if pickerView.tag == 5 {
            bog = bogPickerData[row] as String
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        dataDict["grassland"] = grassland
        dataDict["tillage"] = tillage
        dataDict["urban"] = urban
        dataDict["forest"] = forest
        dataDict["bog"] = bog
        
        let destinationVC = segue.destination as! ViewControllerSite7
        destinationVC.dataDict = self.dataDict
        
    }


}
