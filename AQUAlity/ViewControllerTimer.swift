//
//  ViewControllerTimer.swift
//  AQUAlity
//
//  Created by Nelson on 11/07/2018.
//  Copyright © 2018 Nelson. All rights reserved.
//

import UIKit

class ViewControllerTimer: UIViewController {

    var dataDict = [String: String]()
    
    @IBOutlet weak var timerLbl: UILabel!
    @IBOutlet weak var pauseBtn: UIButton!
    @IBOutlet weak var startBtn: UIButton!
    @IBOutlet weak var resetBtn: UIButton!
    
    var seconds = 180
    var timer = Timer()
    var isTimerRunning = false
    var resumeTapped = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pauseBtn.isEnabled = false

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func startBtnPressed(_ sender: UIButton) {
        if isTimerRunning == false {
            runTimer()
            self.startBtn.isEnabled = false
        }
    }
    
    @IBAction func resetBtnPressed(_ sender: UIButton) {
        timer.invalidate()
        
        seconds = 180
        
        timerLbl.text = timeString(time: TimeInterval(seconds))
        
        isTimerRunning = false
        self.pauseBtn.isEnabled = false
        self.pauseBtn.setTitle("Pause", for: UIControlState.normal)
        self.startBtn.isEnabled = true
    }
    
    @IBAction func pauseBtnPressed(_ sender: UIButton) {
        if self.resumeTapped == false {
            timer.invalidate()
            self.resumeTapped = true
            pauseBtn.setTitle("Resume", for: UIControlState.normal)
        } else {
            runTimer()
            self.resumeTapped = false
            self.pauseBtn.setTitle("Pause", for: UIControlState.normal)
        }
    }

    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(ViewControllerTimer.updateTimer)), userInfo: nil, repeats: true)
        isTimerRunning = true
        pauseBtn.isEnabled = true
    }
    
    @objc func updateTimer() {
        if seconds < 1 {
            timer.invalidate()
            //Send alert
        } else {
            seconds -= 1
            timerLbl.text = timeString(time: TimeInterval(seconds))
        }
    }
    
    // format into mm:ss
    func timeString(time:TimeInterval) -> String {
        
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        
        return String(format: "%d : %d : %d", hours, minutes, seconds)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
                
        let destinationVC = segue.destination as! ViewControllerGroups
        destinationVC.dataDict = self.dataDict
        
    }

}
